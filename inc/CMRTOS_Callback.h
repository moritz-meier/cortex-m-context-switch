#ifndef __CMRTOS_CALLBACK_H
#define __CMRTOS_CALLBACK_H

#define __WEAK __attribute__((weak))

__WEAK void CMRTOS_OutOfHeapCallback(void);

#endif